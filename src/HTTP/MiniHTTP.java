/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTTP;

import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Jesus
 */
public class MiniHTTP 
{
    
    public static void main(String[] args)
    {
        int puerto = 2048;
        String ip = args[0];
        
        if(args.length == 2)
            puerto = Integer.parseInt(args[0]);
        
        try
        {
            MiniHTTP server = new MiniHTTP();
            
            ServerSocket socket = new ServerSocket(puerto);
            System.out.println("Escuchando en el puerto " + puerto + "..." );
            
            Controller controller = new Controller(ip);
            
            while(true)
            {
                Socket cliente = socket.accept();
                
                Thread t = new hiloHTTP(cliente, controller);
                t.start();
            }
            
        } catch (Exception e)
        {
        }
    }
}
