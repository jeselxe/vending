/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTTP;

import java.lang.Exception;
import java.net.Socket;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jesus
 */
public class hiloHTTP extends Thread
{
    private Socket sk;
    private Controller controller;
    private String url;
    
    public hiloHTTP(Socket skCliente, Controller controller)
    {
        sk = skCliente;
        this.controller = controller;
    }

    @Override
    public void run()
    {
        try
        {
            leeURL();
            controller.introducirURL(url);
            
            String respuesta = controller.getWeb();
            
            escribeSocket(respuesta);
            sleep(3000);
            sk.close();
        } catch (Exception ex)
        {
            System.out.println("Error en hiloHTTP: " + ex);
        }
    }
    
    public void leeURL()
    {
        try
        {
        InputStream is = sk.getInputStream();
        DataInputStream flujo =new DataInputStream(is);
        url = flujo.readLine();
        }
        catch (Exception ex)
        {
            System.out.println("Error leyendo socket: " + ex);
        }
    }
    
    public void escribeSocket (String datos)
    {
        try
        {
            OutputStream aux = sk.getOutputStream();
            DataOutputStream flujo= new DataOutputStream( aux );
            flujo.writeUTF(datos);      
        }
        catch (Exception ex)
        {
            System.out.println("Error escribiendo en socket: " + ex.toString());
        }
    }
    
}
