/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTTP;

import Comprobacion.Operario;
import Registro.Registrador;
import Remoto.InterfazRemoto;
import Remoto.Maquina;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;



/**
 *
 * @author Jesus
 */
public class Controller
{
    String version;
    String controlador;
    String metodoHTTP;
    String metodo;
    String param;
    int numMaquina;
    String valor;
    
    String ip;
    
    public Controller(String ip)
    {
        this.ip = ip;
        try
        {
            System.setProperty("java.rmi.server.hostname", ip);
            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
            
            Thread registrador = new Registrador(ip);
            registrador.start();
            
            Operario o = new Operario(ip);
            o.start();
        } 
        catch (Exception e)
        {
            System.out.println("Error creando Servidor RMI: " + e);
        }
        
    }
    
    public void introducirURL(String url)
    {
        String[] partes = url.split(" ");
        
        metodoHTTP = partes[0];
        
        String uri = partes[1];
        String[] uripartido = uri.split("/");

        controlador = uripartido[1];
        if(controlador.equals("Maquinas"))
        {
            if(uripartido.length > 2)
            {
                String parametros = uripartido[2];
                String[] metodoconparam = parametros.split("[?]");
                metodo = metodoconparam[0];
                param = metodoconparam[1];

                String[] atributos = param.split(";");

                for (String s: atributos)
                {
                   if(s.startsWith("maquina="))
                        numMaquina = Integer.parseInt(s.split("=")[1]); 
                    else if(s.startsWith("value="))
                        valor = s.split("=")[1];
                }
            }
            else
            {
                metodo = "index";
            }
        }
        else
            metodo= "icono";
        
        version = partes[2];
    }
    
    public InterfazRemoto getMaquina()
    {

        InterfazRemoto objetoRemoto = null;

        String servidor = "rmi://" + ip + "/" + controlador + "/" + numMaquina;
        
        try
        {
            System.out.println(servidor);
            objetoRemoto = (InterfazRemoto) Naming.lookup(servidor);
            
            System.out.println("Maquina recuperada del servidor");
        }
        catch(Exception ex)
        {
            System.out.println("Error en getMaquina: " + ex); 
        }

        return objetoRemoto;
    }

    public ArrayList<InterfazRemoto> getMaquinas()
    {
        InterfazRemoto objetoRemoto = null;
        ArrayList<InterfazRemoto> maquinas = new ArrayList();

        String servidor = "rmi://" + ip + "/" + controlador;
        try
        {
            System.out.println(servidor);
            String[] url = Naming.list(servidor);
            for (String dir: url){
                System.out.println(dir);
                if(!dir.endsWith("/Registrar")){
                    objetoRemoto = (InterfazRemoto) Naming.lookup(dir);
                    

                    maquinas.add(objetoRemoto);
                }
            }
            System.out.println("Maquina recuperada del servidor");
        }
        catch(Exception ex)
        {
            System.out.println("Error en getMaquinas: " + ex); 
        }

        return maquinas;
    }

    public String getWeb()
    {
        if(metodo.equals("index"))
        {
            ArrayList<InterfazRemoto> maquinas = getMaquinas();
            Web web = new Web(maquinas);
            return web.getHTML();
        }
        else
        {
            InterfazRemoto m = getMaquina();
            Web web = new Web();
            if (m == null)
            {
                return web.ErrorMaquina();
            }
            try
            {
                switch(metodo)
                {
                    case "getTemperatura":
                        web.montarCodigoTemperatura(m);
                        return web.getHTML();
                    case "getMaquina":
                        web.montarCodigoCompleto(m);
                        return web.getHTML();
                    case "getMonedero":
                        web.montarCodigoMonedero(m);
                        return web.getHTML();
                    case "getEstado":
                        web.montarCodigoEstado(m);
                        return web.getHTML();
                    case "getStock":
                        web.montarCodigoStock(m);
                        return web.getHTML();
                    case "setTemperatura":
                        m.setTemperatura(Integer.parseInt(valor));
                        web.montarCodigoCompleto(m);
                        return web.getHTML();
                    case "setEstado":
                        boolean estado = true;
                        if(valor.equals("false"))
                            estado = false;
                        m.setEstado(estado);
                        web.montarCodigoCompleto(m);
                        return web.getHTML();
                    case "setStock":
                        m.setStock(Integer.parseInt(valor));
                        web.montarCodigoCompleto(m);
                        return web.getHTML();
                    case "setMonedero":
                        m.setMonedero(Float.parseFloat(valor));
                        web.montarCodigoCompleto(m);
                        return web.getHTML();
                    default:
                        break;

                }

            }
            catch(Exception ex)
            {
                System.out.println("Error en getWeb: " + ex);
            }

            return web.getHTML();
        }
    }

    
}
