/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HTTP;

import Remoto.InterfazRemoto;
import Remoto.Maquina;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Jesus
 */
public class Web
{
    String codigo;
    Calendar fecha;
    
    public Web()
    {
        
    }

    public Web(InterfazRemoto m)
    {
        montarCodigoCompleto(m);
        
        fecha = new GregorianCalendar();
    }
    
    public void montarCodigoCompleto(InterfazRemoto m)
    {
        montarCodigo(m, true, true, true, true);
    }
    
    public void montarCodigoTemperatura(InterfazRemoto m)
    {
        montarCodigo(m, true, false, false, false);
    }
    
    public void montarCodigoStock(InterfazRemoto m)
    {
        montarCodigo(m, false, true, false, false);
    }
    
    public void montarCodigoMonedero(InterfazRemoto m)
    {
        montarCodigo(m, false, false, true, false);
    }
    
    public void montarCodigoEstado(InterfazRemoto m)
    {
        montarCodigo(m, false, false, false, true);
    }
    
    public Web(ArrayList<InterfazRemoto> lista)
    {
        indice(lista);
    }

    public void indice(ArrayList<InterfazRemoto> maquinas)
    {
        try{
            codigo =
                        "<HTML>" +
                        "    <HEAD>" +
                        "       <TITLE>Maquina dispensadora</TITLE>" +
                        "       <STYLE>" +
                                    css() +
                        "       </STYLE>" +
                        "    </HEAD>" +
                        "    <BODY>";

            for(InterfazRemoto m: maquinas)
            {
            codigo +=   "        <a href='/Maquinas/getMaquina?maquina=" + m.getNombre() +"' class='button anchor bck light text center'>" +
                        "           Maquina " + m.getNombre() +
                        "       </a>" +
                        "      <SECTION class='padding text bold color theme'>" +
                        "            <div class='row'>" +
                        "                <DIV class ='column_6'>" +
                        "                    Temperatura" +
                        "                </DIV>" +
                        "                <DIV class='offset_6'>" + m.getTemperatura() + "</DIV>" +
                        "            </div>" +
                        "            <div class='row'>" +
                        "                <DIV class ='column_6'>" +
                        "                    Stock" +
                        "                </DIV>" +
                        "                <DIV class='offset_6'>" + m.getStock() + "</DIV>" +
                        "            </div>" +
                        "            <div class='row'>" +
                        "                <DIV class ='column_6'>" +
                        "                    Monedero" +
                        "                </DIV>" +
                        "                <DIV class='offset_6'>" + m.getMonedero() + "</DIV>" +
                        "            </div>" +
                        "            <div class='row'>" +
                        "                <DIV class ='column_6'>" +
                        "                    Estado" +
                        "                </DIV>" +
                        "                <DIV class='offset_6'>" + (m.getEstado() ? "ACTIVA" : "INACTIVA") + "</DIV>" +
                        "            </div>" +
                        "            <div class='row'>" +
                        "                <DIV class ='column_6'>" +
                        "                    IP" +
                        "                </DIV>" +
                        "                <DIV class='offset_6'>" + m.getIP() + "</DIV>" +
                        "            </div>" +                    
                        "        </SECTION>";
            }
            
            if(maquinas.isEmpty())
                codigo +="        <HEADER class='padding bck light text center'>" +
                         "           No hay maquinas registradas"  +
                         "       </HEADER>";
            
            codigo +=   "    </BODY>" +
                        "</HTML>";
        }
        catch(Exception ex)
        {
            System.out.println("Error creando HTML: " + ex);
        }
    }

    public void montarCodigo(InterfazRemoto m, boolean temp, boolean stock, boolean monedero, boolean estado)
    {
        try{
            codigo =
                        "<HTML>" +
                        "    <HEAD>" +
                        "       <TITLE>Maquina dispensadora</TITLE>" +
                        "       <STYLE>" +
                                    css() +
                        "       </STYLE>" +
                        "    </HEAD>" +
                        "       <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>" +
                        "       <script type='text/javascript'>" +
                                    script(m.getNombre()) +
                        "       </script>" +
                        "    <BODY>" +
                        "        <HEADER class='padding bck light text center'>" +
                        "           Maquina " + m.getNombre() +
                        "       </HEADER>" +
                        "      <SECTION class='padding text bold color theme'>" +
                        "<div class='row'>" +
			"	<div class='column_4'>" +
			"		<div class='row'>" +
			"			<div class='text center bold'> GETTERS </div>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<div class='column_4'>" +
			"				<a href='/Maquinas/getTemperatura?maquina=" + m.getNombre() + "' class='color white'>getTemperatura</a>" +
			"			</div>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<div class='column_4'>" +
			"				<a href='/Maquinas/getStock?maquina=" + m.getNombre() + "' class='color white'>getStock</a>" +
			"			</div>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<div class='column_4'>" +
			"				<a href='/Maquinas/getMonedero?maquina=" + m.getNombre() + "' class='color white'>getMonedero</a>" +
			"			</div>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<div class='column_4'>" +
			"				<a href='/Maquinas/getEstado?maquina=" + m.getNombre() + "' class='color white'>getEstado</a>" +
			"			</div>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<div class='text center bold'> SETTERS </div>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<form>" +
			"				<div class='column_2'>" +
			"					<input type='button' class='set' value='setTemperatura'>" +
			"				</div>" +
			"				<div class='column_2'>" +
			"					<input type='number' id='setTemperatura' value='" + m.getTemperatura() + "'>" +
			"				</div>" +
			"			</form>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<form>" +
			"				<div class='column_2'>" +
			"					<input type='button' class='set' value='setStock'>" +
			"				</div>" +
			"				<div class='column_2'>" +
			"					<input type='number' id='setStock' value='" + m.getStock()+ "'>" +
			"				</div>" +
			"			</form>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<form>" +
			"				<div class='column_2'>" +
			"					<input type='button' class='set' value='setMonedero'>" +
			"				</div>" +
			"				<div class='column_2'>" +
			"					<input type='number' id='setMonedero' value='" + m.getMonedero()+ "'>" +
			"				</div>" +
			"			</form>" +
			"		</div>" +
			"		<div class='row'>" +
			"			<form>" +
			"				<div class='column_2'>" +
			"					<input type='button' class='set' value='setEstado'>" +
			"				</div>" +
			"				<div class='column_1'>" +
			"					ACTIVA <input " + (m.getEstado() ? "checked " : "") + "type='radio' name='radio' value='true'> " +
			"				</div>" +
			"				<div class='column_1'>" +
			"					INACTIVA <input " + (m.getEstado() ? "" : "checked ") + " type='radio' name='radio' value='false'>" +
			"				</div>" +
			"			</form>" +
			"		</div>" +					
			"	</div>" +
                        "       <div class='column_8 text bold'>";
            if(temp)
            {            
            codigo +=   "            <div class='row'>" +
                        "               <div class='margin-top'>" +
                        "                   <DIV class ='offset_2 column_3'>" +
                        "                    Temperatura" +
                        "                </DIV>" +
                        "                <DIV class='column_3'>" + m.getTemperatura() + "</DIV>" +
                        "               </div>" +
                        "            </div>";
            }
            if(stock)
            {
            codigo +=   "            <div class='row'>" +
                        "               <div class='margin-top'>" +
                        "                <DIV class ='offset_2 column_3'>" +
                        "                    Stock" +
                        "                </DIV>" +
                        "                <DIV class='column_3'>" + m.getStock() + "</DIV>" +
                        "               </div>" +
                        "            </div>";
            }
            if(monedero)
            {
            codigo +=   "            <div class='row'>" +
                        "               <div class='margin-top'>" +
                        "                <DIV class ='offset_2 column_3'>" +
                        "                    Monedero" +
                        "                </DIV>" +
                        "                <DIV class='column_3'>" + m.getMonedero() + "</DIV>" +
                        "               </div>" +
                        "            </div>";
            }
            if(estado)
            {
            codigo +=   "            <div class='row'>" +
                        "               <div class='margin-top'>" +
                        "                <DIV class ='offset_2 column_3'>" +
                        "                    Estado" +
                        "                </DIV>" +
                        "                <DIV class='column_3'>" + (m.getEstado() ? "ACTIVA" : "INACTIVA") + "</DIV>" +
                        "               </div>" +
                        "            </div>";
            }

            codigo +=   "            <div class='row'>" +
                        "               <div class='margin-top'>" +
                        "                <DIV class ='offset_2 column_3'>" +
                        "                    IP" +
                        "                </DIV>" +
                        "                <DIV class='column_3'>" + m.getIP() + "</DIV>" +
                        "               </div>" +
                        "            </div>" + 
                        "            <div class='row'>" +
                        "               <div class='margin-top'>" +
                        "                <DIV class ='offset_2 column_3'>" +
                        "                    Fecha" +
                        "                </DIV>" +
                        "                <DIV class='column_3'>" + getFecha() + "</DIV>" +
                        "               </div>" +
                        "            </div>" + 
                        "           <div class='row'>" +
                        "              <div class='column_8'> " +
                        "                  <div class='padding bck light text center margin-top'>" +
                                              m.getDisplay() +
                        "                  </div>" +
                        "              </div>" +
                        "          </div>" +
                        "       </div>" +
                        "       </div> " +
                        "       </SECTION>" +
                        "       <SECTION>" +
                        "           <DIV class='row'>" +
                        "               <div class='offset_3 column_3'>" +
                        "                   <a href='/Maquinas/getMaquina?maquina=" + m.getNombre() + "' id='refresh' class='button anchor bck light text center color theme'> Refrescar maquina </a>" +
                        "               </div>" +
                        "               <div class='column_3'>" +
                        "                   <a href='/Maquinas' id='refresh' class='button anchor bck light text center color theme'> Volver al Menu </a>" +
                        "               </div>" +
                        "           </div> " +
                        "       </SECTION>" +
                        "    </BODY>" +
                        "</HTML>";
        }
        catch(Exception ex)
        {
            System.out.println("Error creando HTML: " + ex);
        }
    }
    
    public String getFecha()
    {
        
        Date now = new Date();
        
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss ");
        String f = format.format(now);
        
        return f;
    }

    public String getHTML(){
        String cabecera =   "HTTP/1.1 200 OK\n" +
                            "Connection: close\n" +
                            "\n";
        return cabecera + codigo;
    }
    
    public String ErrorMaquina()
    {
        codigo = 
                "<HTML>" +
                "    <HEAD>" +
                "       <TITLE>Maquina dispensadora</TITLE>" +
                "       <STYLE>" +
                            css() +
                "       </STYLE>" +
                "    </HEAD>" +
                "    <BODY>" +
                "        <HEADER class='padding bck light text center'>" +
                "           Error" +
                "       </HEADER>" +
                "      <SECTION class='padding text bold color theme'>" +
                "           <div class='text center'>La maquina seleccionada no esta registrada</div>" +
                "        </SECTION>" +
                "    </BODY>" +
                "</HTML>";



        return getHTML(); 
    }

    public String css(){
        String css = 
                ".row{margin:0 auto;width:960px;overflow:hidden;display:block;}" +
		"		.row .row{margin:0 -16px 0 -16px;width:auto;display:inline-block}" +
		"		[class^='column_'],[class*=' column_']{margin:0 16px 0 16px;float:left;display:inline}" +
		"		.column_1{width:48px}" +
		"		.column_2{width:128px}" +
		"		.column_3{width:208px}" +
		"		.column_4{width:288px}" +
		"		.column_5{width:368px}" +
		"		.column_6{width:448px}" +
		"		.column_7{width:528px}" +
		"		.column_8{width:608px}" +
		"		.column_9{width:688px}" +
		"		.column_10{width:768px}" +
		"		.column_11{width:848px}" +
		"		.column_12{width:928px}" +
		"		.offset_1{margin-left:96px}" +
		"		.offset_2{margin-left:176px}" +
		"		.offset_3{margin-left:256px}" +
		"		.offset_4{margin-left:336px}" +
		"		.offset_5{margin-left:416px}" +
		"		.offset_6{margin-left:496px}" +
		"		.offset_7{margin-left:576px}" +
		"		.offset_8{margin-left:656px}" +
		"		.offset_9{margin-left:736px}" +
		"		.offset_10{margin-left:816px}" +
		"		.offset_11{margin-left:896px}" +
		"		.show-phone{display:none !important}" +
		"		.show-tablet{display:none !important}" +
		"		.show-screen{display:inherit !important}" +
		"		.hide-phone{display:inherit !important}" +
		"		.hide-tablet{display:inherit !important}" +
		"		.hide-screen{display:none !important}" +
		"		@media only screen and (min-width:1200px){.row{width:1200px;}" +
		"		.row .row{margin:0 -20px 0 -20px}" +
		"		[class^='column_'],[class*=' column_']{margin:0 20px 0 20px}" +
		"		.column_1{width:60px}" +
		"		.column_2{width:160px}" +
		"		.column_3{width:260px}" +
		"		.column_4{width:360px}" +
		"		.column_5{width:460px}" +
		"		.column_6{width:560px}" +
		"		.column_7{width:660px}" +
		"		.column_8{width:760px}" +
		"		.column_9{width:860px}" +
		"		.column_10{width:960px}" +
		"		.column_11{width:1060px}" +
		"		.column_12{width:1160px}" +
		"		.offset_1{margin-left:120px}" +
		"		.offset_2{margin-left:220px}" +
		"		.offset_3{margin-left:320px}" +
		"		.offset_4{margin-left:420px}" +
		"		.offset_5{margin-left:520px}" +
		"		.offset_6{margin-left:620px}" +
		"		.offset_7{margin-left:720px}" +
		"		.offset_8{margin-left:820px}" +
		"		.offset_9{margin-left:920px}" +
		"		.offset_10{margin-left:1020px}" +
		"		.offset_11{margin-left:1120px}" +
		"		.show-phone{display:none !important}" +
		"		.show-tablet{display:none !important}" +
		"		.show-screen{display:inherit}" +
		"		.hide-phone{display:inherit !important}" +
		"		.hide-tablet{display:inherit !important}" +
		"		.hide-screen{display:none !important}" +
		"		}@media only screen and (min-width: 768px) and (max-width: 959px){.row{width:768px;}" +
		"		.row .row{margin:0 -14px 0 -14px}" +
		"		[class^='column_'],[class*=' column_']{margin:0 14px 0 14px}" +
		"		.column_1{width:36px}" +
		"		.column_2{width:100px}" +
		"		.column_3{width:164px}" +
		"		.column_4{width:228px}" +
		"		.column_5{width:292px}" +
		"		.column_6{width:356px}" +
		"		.column_7{width:420px}" +
		"		.column_8{width:484px}" +
		"		.column_9{width:548px}" +
		"		.column_10{width:612px}" +
		"		.column_11{width:676px}" +
		"		.column_12{width:740px}" +
		"		.offset_1{margin-left:78px}" +
		"		.offset_2{margin-left:142px}" +
		"		.offset_3{margin-left:206px}" +
		"		.offset_4{margin-left:270px}" +
		"		.offset_5{margin-left:334px}" +
		"		.offset_6{margin-left:398px}" +
		"		.offset_7{margin-left:462px}" +
		"		.offset_8{margin-left:526px}" +
		"		.offset_9{margin-left:590px}" +
		"		.offset_10{margin-left:654px}" +
		"		.offset_11{margin-left:718px}" +
		"		.show-phone{display:none !important}" +
		"		.show-tablet{display:inherit !important}" +
		"		.show-screen{display:none !important}" +
		"		.hide-phone{display:inherit !important}" +
		"		.hide-tablet{display:none !important}" +
		"		.hide-screen{display:inherit !important}" +
		"		}@media only screen and (max-width: 767px){.row{width:300px;}" +
		"		.row .row{margin:0}" +
		"		[class^='column_'],[class*=' column_']{width:300px;margin:10px 0 0 0}" +
		"		.offset_1,.offset_2,.offset_3,.offset_4,.offset_5,.offset_6,.offset_7,.offset_8,.offset_9,.offset_10,.offset_11{margin-left:0}" +
		"		.show-phone{display:inherit !important}" +
		"		.show-tablet{display:none !important}" +
		"		.show-screen{display:none !important}" +
		"		.hide-phone{display:none !important}" +
		"		.hide-tablet{display:inherit !important}" +
		"		.hide-screen{display:inherit !important}" +
		"		}@media only screen and (min-width: 480px) and (max-width: 767px){.row{margin:0 auto;width:456px}" +
		"		.row .row{margin:0;width:auto;display:inline-block}" +
		"		[class^='column_'],[class*=' column_']{margin:10px 0 0 0;width:456px}" +
		"		.show-phone,.hide-tablet,.hide-screen{display:inherit !important}" +
		"		.show-tablet,.show-screen,.hide-phone{display:none !important}" +
		"		}" +
                "               .padding{padding:1em !important}" +
                "                body{color:#ddd;background:black;font-size:16px;font-family:'Roboto','Helvetica Neue',Helvetica,sans-serif;font-weight:300;}" +
                "                body a{color:#222;}" +
                "                body a:not(.button):hover,body a:not(.button):active{color:#4bc613}" +
                "                body .bck.light{background-color:#ddd; color:black;}" +
                "                body .bck.dark{background-color:black}" +
                "                body .bck.theme{background-color:#4bc613}" +
                "                body .bck.theme,body .bck.dark{color:#fff;}" +
                "                body .bck.theme a,body .bck.dark a{color:#fff;}" +
                "                body .bck.theme a:not(.button):hover,body .bck.dark a:not(.button):hover,body .bck.theme a:not(.button):active,body .bck.dark a:not(.button):active{color:#fff}" +
                "                body .text.shadow{-webkit-text-shadow:0 1px 1px rgba(0,0,0,0.5);-moz-text-shadow:0 1px 1px rgba(0,0,0,0.5);-ms-text-shadow:0 1px 1px rgba(0,0,0,0.5);-o-text-shadow:0 1px 1px rgba(0,0,0,0.5);text-shadow:0 1px 1px rgba(0,0,0,0.5)}" +
                "                body .color.white{color:#fff !important}" +
                "                body .color:not(.bck).theme{color:#4bc613 !important}" +
                "                body .color.default{color:#222 !important}" +
                "                body strong{font-weight:700 !important}" +
                "                .text.bold{font-weight:700}" +
                "                .text.center{text-align:center}" +
                "                .margin{margin:1em}" +
                "                .margin-top{margin-top:1em}" +
                "                .button,button{position:relative;display:inline-block;border:none;cursor:pointer;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:2.5em;text-align:center;text-decoration:none;outline:none;background:none;-webkit-transition:background-color 400ms;-moz-transition:background-color 400ms;-ms-transition:background-color 400ms;-o-transition:background-color 400ms;transition:background-color 400ms;}" +
                "                .button.anchor,button.anchor{width:100%;}";
        
        return css;
    }    
    
    public String script(String maquina)
    {
        String script = 
			"$(document).ready(function() { \n" + 
			"	$('.set').click(function() { \n" +
			"		var sender = $(this);\n" +
			"		if(sender.val() == 'setEstado') {\n" +
			"			var value = $(\"form input[type='radio']:checked\").val();\n" +
			"			if($(\"form input[type='radio']\").is(':checked')) {\n" +
			"			window.location.href = '/Maquinas/' + sender.val() +'?maquina=" + maquina + ";value=' + value;\n" +
			"			}else{\n" +
			"				alert(' Selecciona una opcion ');\n" +
			"			}\n" +
			"		}\n" +
			"		else{\n" +
			"			var value = $('#' + sender.val() ).val();\n" +
			"			window.location.href = '/Maquinas/' + sender.val() +'?maquina=" + maquina + ";value=' + value;\n" +
			"		}\n" +
			"	});\n" +
                        "       $('#refresh').click(function() { \n" +
                        "           window.location.href = '/Maquinas/getMaquina?maquina=" + maquina + "'; \n" +
                        "       });\n" +
                        "       $('#menu').click(function() { \n" +
                        "           window.location.href = '/Maquinas'; \n" +
                        "       });\n" +
			"});\n";                
        return script;
    }
}
