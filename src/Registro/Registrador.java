/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registro;

import Remoto.InterfazRemoto;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.Remote;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Jesus
 */
public class Registrador extends Thread implements InterfazRegistro, Remote
{
    String ip;
    public Registrador(String ip)
    {
        this.ip = ip;
    }
    
    @Override
    public void run()
    {
        String URLRegistro = "/Registrar";
                
        try
        {
            System.setProperty("java.rmi.server.hostname", ip);
            
            InterfazRegistro stub = (InterfazRegistro)UnicastRemoteObject.exportObject(this, 0);

            Naming.rebind(URLRegistro, stub);
        }
        catch (Exception e)
        {
            System.out.println("Error registrando el Registrador: " + e);
        }
        
        String espera = new String();
        synchronized (espera) 
        {
            try
            {
                espera.wait();
            } catch (InterruptedException ex)
            {
                System.out.println("Error en el proceso del Registrador: " + ex);
            }
        }
    }
    
    

    @Override
    public void registrarMaquina(InterfazRemoto maquina) throws java.rmi.RemoteException
    {
        String URLRegistro;
        System.out.println("Registrar maquina " + maquina.getNombre() );
        try
        {
            URLRegistro = "/Maquinas/" + maquina.getNombre();
            
            InterfazRemoto stub = (InterfazRemoto)UnicastRemoteObject.exportObject(maquina, 0);
            
            Naming.rebind(URLRegistro, maquina);
        } 
        catch (Exception e)
        {
            System.out.println("Error registrando Maquina: " + e);
        }
    }

    @Override
    public String getIP() throws java.rmi.RemoteException
    {
       return ip;
    }
    
    
}
