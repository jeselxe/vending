/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registro;

import Remoto.InterfazRemoto;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Jesus
 */
public interface InterfazRegistro extends Remote
{
    public void registrarMaquina(InterfazRemoto maquina) throws RemoteException;
    public String getIP() throws java.rmi.RemoteException;
}
