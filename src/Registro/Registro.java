/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Registro;

import Remoto.InterfazRemoto;
import Remoto.Maquina;
import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Jesus
 */
public class Registro 
{
    // java Registro <ip servidor> <Nombre maquina> <ip local>
    public static void main(String[] args)
    {
        String servidor = "172.27.186.169";//String servidor = args[0];
        try
        {
            String ip = "172.27.186.169";//String ip = args[2];
            // Poner la ip de la maquina
            System.setProperty("java.rmi.server.hostname", ip);
            
            // Crea la maquina a registrar
            Maquina m = new Maquina(1, ip);//Maquina m = new Maquina(Integer.parseInt(args[1]), ip);
            
            String serverURL = "rmi://" + servidor + "/Registrar";
            // Busca el objeto servidor
            InterfazRegistro registro = (InterfazRegistro) Naming.lookup(serverURL);
            
            System.out.println("maquina preparada para registrar en registrador de IP: " + registro.getIP());
            // El servidor registra la maquina
            registro.registrarMaquina(m);
            
            System.out.println("Maquina Registrada!");
        } 
        catch (Exception e)
        {
            System.out.println("Error en Registro: " + e);
        }
    }
    
}
