/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Comprobacion;

import Remoto.InterfazRemoto;
import java.rmi.Naming;
import java.rmi.RemoteException;

/**
 *
 * @author Jesus
 */
public class Operario extends Thread
{
    String server;
    
    public Operario(String dirServer)
    {
        server = dirServer;
    }
    
    public void comprobarValores(InterfazRemoto m)
    {
        try
        {
            if(m.getEstado())
            {
                
                if(m.getTemperatura() > 10)
                {
                    m.setDisplay("Maquina refrigerandose");
                    m.setEstado(false);
                }
                if(m.getMonedero() > 800 || m.getMonedero() == 0)
                {
                    m.setDisplay("Maquina sin cambio");
                    m.setEstado(false);
                }
                if (m.getStock() < 10)
                {
                    m.setDisplay("Maquina sin Stock");
                    m.setEstado(false);
                }
            }
            else
            {
                if((m.getTemperatura() <= 10) && 
                    m.getMonedero() <= 800 && m.getMonedero() > 0 && 
                    (m.getStock() >= 10))
                {
                    m.setDisplay("Maquina operativa");
                    m.setEstado(true);
                }               
            }
        } catch (Exception ex)
        {
            System.out.println("Error comprobando valores: " + ex);
        }
    }
    
    @Override
    public void run()
    {
        try
        {
            String servidorURL = "rmi://" + server + "/Maquinas";
            
            while (true)
            {
                String[] direcciones = Naming.list(servidorURL);

                for(String dir: direcciones)
                {
                    if(!dir.endsWith("/Registrar"))
                    {
                        InterfazRemoto m = (InterfazRemoto) Naming.lookup(dir);

                        comprobarValores(m);
                    }
                }
                    
                sleep(2000);

            }
        } catch (InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        } catch (Exception ex)
        {
            System.out.println("Error en Operario: " + ex);
        }
    }
    
}
