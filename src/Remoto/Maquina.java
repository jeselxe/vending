/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Remoto;

import java.io.Serializable;
import java.rmi.*;
import java.rmi.server.*;

/**
 *
 * @author Jesus
 */
public class Maquina extends UnicastRemoteObject
implements InterfazRemoto, Serializable
{
    private String nombre;
    private String ip;
    private int temperatura;
    private int stock;
    private float monedero;
    private boolean estado;
    private String display;

    public Maquina() throws RemoteException
    {
        super();
    }

    public Maquina(int name, String ip) throws RemoteException
    {
        super();
        nombre = "" + name;
        temperatura = 9;
        stock = 10;
        monedero = 50;
        estado = true;
        this.ip = ip;
        display = "Maquina operativa";
    }

    public Maquina (String nombre, String ip , float monedero, int stock, int temperatura, boolean estado) throws RemoteException 
    {
        this.ip = ip;
        setMonedero(monedero);
        setNombre(nombre);
        setStock(stock);
        setEstado(estado);
        setTemperatura(temperatura);
    }
    
    @Override
    public float getMonedero()  throws RemoteException 
    {
        return monedero;
    }

    @Override
    public void setMonedero(float m)  throws RemoteException
    {
        if (m <= 1000 && m >= 0)
        {
            monedero = m;
            System.out.println("Monedero actualizado");
            
        }
    }

    @Override
    public String getNombre()  throws RemoteException
    {
        return nombre;
    }

    @Override
    public void setNombre(String n)  throws RemoteException
    {
        nombre = n;
    }

    @Override
    public int getStock()  throws RemoteException
    {
        return stock;
    }

    @Override
    public void setStock(int s)  throws RemoteException
    {
        if(s >= 0 && s <= 60)
        {
            stock = s;
            System.out.println("Stock actualizado");
        }
    }

    @Override
    public void setEstado(boolean e)  throws RemoteException
    {
        estado = e;
        System.out.println("Estado actualizado");
    }

    @Override
    public boolean getEstado()  throws RemoteException
    {
        return estado;
    }

    @Override
    public int getTemperatura()  throws RemoteException
    {
        return temperatura;
    }

    @Override
    public void setTemperatura(int t)  throws RemoteException
    {
        if (t <= 50 && t >= 0)
        {
            temperatura = t;
            System.out.println("Temperatura actualizada");
        }
    }

    @Override
    public Maquina getMaquina()  throws RemoteException
    {
        return this;
    }

    @Override
    public void setMaquina(String nombre, String ip, float monedero, int stock, int temperatura, boolean estado) throws RemoteException 
    {
        setMonedero(monedero);
        setNombre(nombre);
        setStock(stock);
        setEstado(estado);
        setTemperatura(temperatura);
    }
    
    @Override
    public String getDisplay()  throws RemoteException
    {
        return display;
    }

    @Override
    public void setDisplay(String msg)  throws RemoteException
    {
        display = msg;
    }    
    
    @Override
    public String getIP() throws RemoteException
    {
        return ip;
    }
    
}
