/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Remoto;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Jesus
 */
public interface InterfazRemoto extends Remote
{
    public float getMonedero() throws RemoteException;

    public void setMonedero(float monedero) throws RemoteException;

    public String getNombre() throws RemoteException;

    public void setNombre(String nombre) throws RemoteException;

    public int getStock() throws RemoteException;

    public void setStock(int stock) throws RemoteException;

    public void setEstado(boolean estado) throws RemoteException;

    public boolean getEstado() throws RemoteException;

    public int getTemperatura() throws RemoteException;

    public void setTemperatura(int temperatura) throws RemoteException;
    
    public Maquina getMaquina() throws RemoteException;

    public void setMaquina(String nombre, String ip, float monedero, int stock, int temperatura, boolean estado) throws RemoteException;

    public String getDisplay()  throws RemoteException;

    public void setDisplay(String msg)  throws RemoteException;    
    
    public String getIP() throws RemoteException;
}
